use json;
use std::collections::HashMap;

#[derive(Deserialize,Debug)]
pub struct LoginResponse {
    pub access_token: String,
    pub device_id: String,
    pub home_server: String,
    pub refresh_token: String,
    pub user_id: String,
}

#[derive(Deserialize,Debug)]
pub struct ErrorResponse {
    pub errcode: String,
    pub error: String,
}

#[derive(Deserialize,Debug)]
pub struct SyncResponse {
    pub next_batch: String,
    pub rooms: RoomsResponse,
}

#[derive(Deserialize,Debug)]
pub struct RoomsResponse {
    pub join: HashMap<String, JoinResponse>,
}

#[derive(Deserialize,Debug)]
pub struct JoinResponse {
    pub state: StateResponse,
    pub timeline: TimelineResponse,
}

#[derive(Deserialize,Debug)]
pub struct StateResponse {
    pub events: Vec<EventResponse>,
}

#[derive(Deserialize,Debug)]
pub struct TimelineResponse {
    pub events: Vec<EventResponse>,
}

#[derive(Deserialize,Debug)]
pub struct EventResponse {
    #[serde(rename="type")]
    pub typ: String,
    pub content: json::Value,
    pub sender: String,
    pub state_key: Option<String>,
}
