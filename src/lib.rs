#![doc(html_root_url="https://panicbit.gitlab.io/matrixc/doc/matrixc/")]
#![feature(proc_macro)]
extern crate hyper;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json as json;
#[macro_use]
extern crate try_opt;

use std::error;
use std::rc::Rc;
use hyper::mime;

mod response;
use response::*;

mod events;
pub use events::*;

mod state;
pub use state::*;

pub type Result<T> = ::std::result::Result<T, Box<error::Error>>;

const JSON_MIME: mime::Mime<[(mime::Attr, mime::Value);0]> = mime::Mime(
    mime::TopLevel::Application,
    mime::SubLevel::Json,
    []
);

pub struct Client {
    http: hyper::Client,
    server: String,
    base_url: String,
    refresh_token: String,
    access_token: String,
    transaction_id: u64,
}

impl Client {
    pub fn login_with_password(server: &str, user: &str, password: &str) -> Result<Client> {
        let mut http = hyper::Client::new();
        let timeout = ::std::time::Duration::from_secs(3*60);
        http.set_read_timeout(Some(timeout));
        http.set_write_timeout(Some(timeout));

        let base_url = format!("https://{server}/_matrix", server = server);
        let login_url = format!("{base}/client/r0/login", base = base_url);
        let request = LoginRequest {
            typ: "m.login.password",
            user: user,
            password: password,
        };
        let request = try!(json::to_string(&request));
        let response = try!(http.post(&login_url).body(&request).send());
        let response: LoginResponse = try!(deserialize_response(response));

        Ok(Client {
            http: http,
            server: server.to_owned(),
            base_url: format!("https://{}/_matrix", server),
            refresh_token: response.refresh_token,
            access_token: response.access_token,
            transaction_id: 0,
        })
    }

    pub fn sync(&self, next_batch: Option<&str>) -> Result<SyncResponse> {
        let mut sync_url = format!(
            "{base}/client/r0/sync?access_token={access_token}&timeout=30000",
            base = self.base_url,
            access_token = self.access_token,
        );
        if let Some(next_batch) = next_batch {
            sync_url.push_str("&since=");
            sync_url.push_str(next_batch);
        }
        let response = try!(self.http.get(&sync_url).send());

        let response: SyncResponse = try!(deserialize_response(response));

        Ok(response)
    }

    pub fn send_notice(&mut self, room_id: &str, text: &str) -> Result<()> {
        let mut event_url = format!(
            "{base}/client/r0/rooms/{room_id}/send/{event_type}/{transaction_id}?access_token={access_token}",
            base = self.base_url,
            access_token = self.access_token,
            transaction_id = self.transaction_id,
            room_id = room_id,
            event_type = "m.room.message",
        );

        #[derive(Serialize)]
        struct Content<'a> {
            msgtype: &'a str,
            body: &'a str,
        }

        let content = Content {
            msgtype: "m.notice",
            body: text,
        };
        let content = try!(json::to_string(&content));

        let send_result = self.http.put(&event_url).body(&content).send();
        self.transaction_id += 1;
        try!(send_result);

        Ok(())
    }

    pub fn run<H: Handler>(mut self, mut handler: H) -> Result<()> {
        let mut next_batch: Option<String> = None;
        let mut sync;
        let mut state = State::default();

        loop {
            sync = try!(self.sync(next_batch.as_ref().map(String::as_str)));
            next_batch = Some(sync.next_batch);

            for (room_id, room) in sync.rooms.join {
                let room_id = Rc::new(room_id);
                let room_state = state.get_room_state_mut(room_id.clone());

                println!("\n=== State of {} ===", room_id);
                for event in room.state.events {
                    // println!("{:?}", event);
                    match RoomEvent::from(event.typ, event.state_key, event.content) {
                        Some(RoomEvent::Member(member)) => {
                            println!("Member event: {:?}", member);
                            room_state.update_member(member);
                            println!("==> Updated state: {:#?}", room_state);
                        },
                        _ => {}
                    }
                }
                println!("=== End of state\n");

                for event in room.timeline.events {
                    // println!("{:?}", event);
                    // let (sender, room_event) = RoomEvent::from(event);
                    match RoomEvent::from(event.typ, event.state_key, event.content) {
                        Some(RoomEvent::Message(message)) => handler.message(HandlerMessage {
                            client: &mut self,
                            room_id: room_id.clone(),
                            room_state: room_state,
                            sender: event.sender,
                            message: message,
                        }),
                        Some(RoomEvent::Member(member)) => {
                            println!("Member event: {:?}", member);
                            room_state.update_member(member);
                            println!("==> Updated state: {:#?}", room_state);
                        },
                        _ => {}
                    }
                }
            }
        }
    }
}

pub struct HandlerMessage<'a> {
    pub client: &'a mut Client,
    pub room_id: Rc<String>,
    pub room_state: &'a RoomState,
    pub sender: String,
    pub message: MessageEvent,
}

fn deserialize_response<ResponseT: serde::Deserialize>(response: hyper::client::Response) -> Result<ResponseT> {
    if response.status.is_success() {
        let response: ResponseT = try!(json::from_reader(response));
        Ok(response)
    }
    else {
        let response: ErrorResponse = try!(json::from_reader(response));
        Err(format!("[{}] {}", response.errcode, response.error).into())
    }
}

#[derive(Serialize)]
struct LoginRequest<'a> {
    #[serde(rename="type")]
    typ: &'static str,
    user: &'a str,
    password: &'a str,
}

pub trait Handler {
    fn message(&mut self, _message: HandlerMessage) {}
}
