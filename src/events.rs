use json;

#[derive(Debug)]
pub enum RoomEvent {
    Aliases,
    CanonicalAlias,
    Create,
    JoinRules,
    Member(MemberEvent),
    PowerLevels,
    Redaction,
    Message(MessageEvent),
    Name,
    Topic,
    Avatar,
    Unknown(String),
}

impl RoomEvent {
    pub fn from(typ: String, state_key: Option<String>, content: json::Value) -> Option<Self> {
        Some(match typ.as_str() {
            "m.room.aliases"         => RoomEvent::Aliases,
            "m.room.canonical_alias" => RoomEvent::CanonicalAlias,
            "m.room.create"          => RoomEvent::Create,
            "m.room.join_rules"      => RoomEvent::JoinRules,
            "m.room.member"          => RoomEvent::Member(try_opt!(MemberEvent::from(try_opt!(state_key), content))),
            "m.room.power_levels"    => RoomEvent::PowerLevels,
            "m.room.redaction"       => RoomEvent::Redaction,
            "m.room.message"         => RoomEvent::Message(try_opt!(MessageEvent::from(content))),
            "m.room.name"            => RoomEvent::Name,
            "m.room.topic"           => RoomEvent::Topic,
            "m.room.avatar"          => RoomEvent::Avatar,
            _ => RoomEvent::Unknown(typ)
        })
    }
}

#[derive(Debug,Deserialize)]
pub struct MemberEvent {
    pub avatar_url: Option<String>,
    #[serde(rename="displayname")]
    pub display_name: Option<String>,
    pub membership: Membership,
    pub user: String,
}

#[derive(Debug,Copy,Clone,Eq,PartialEq,Deserialize)]
pub enum Membership {
    #[serde(rename="join")] Join,
    #[serde(rename="invite")] Invite,
    #[serde(rename="leave")] Leave,
    #[serde(rename="ban")] Ban,
    #[serde(rename="knock")] Knock,
}

impl MemberEvent {
    fn from(state_key: String, mut content: json::Value) -> Option<Self> {
        try_opt!(content.as_object_mut()).insert("user".into(), json::Value::String(state_key));
        json::from_value(content).ok()
    }
}

#[derive(Debug)]
pub enum MessageEvent {
    Text(String),
    Unknown(json::Value)
}

impl MessageEvent {
    fn from(content: json::Value) -> Option<Self> {
        let msgtype = try_opt!(content.find("msgtype").and_then(|v| v.as_str()));

        Some(match msgtype {
            "m.text" => {
                let body = try_opt!(content.find("body").and_then(|v| v.as_str()));
                MessageEvent::Text(body.to_string())
            },
            _ => MessageEvent::Unknown(content.clone()),
        })
    }
}

#[derive(Debug)]
enum MessageType {
    Text,
    Emote,
    Notice,
    Image,
    File,
    Location,
    Video,
    Audio,
    Unknown(String),
}
