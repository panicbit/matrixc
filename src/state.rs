use std::rc::Rc;
use std::collections::{
    HashMap,
    HashSet,
};
use events::{
    Membership,
    MemberEvent,
};

#[derive(Debug,Default)]
pub struct State {
    rooms: HashMap<Rc<String>, RoomState>,
}

impl State {
    pub fn get_room_state_mut(&mut self, room: Rc<String>) -> &mut RoomState {
        self.rooms.entry(room).or_insert_with(RoomState::default)
    }
}

#[derive(Debug,Default)]
pub struct RoomState {
    name: Option<String>,
    topic: Option<String>,
    avatar: Option<String>,
    users: UserList,
}

impl RoomState {
    /// Returns the disambiguated display name of `user_id` i.e. one of three things:
    ///
    /// - The display name if it was found and noone else uses it
    /// - The `user_id` if no display name is found
    /// - The display name in the form of `display name (user id)` when the name is already used
    pub fn get_display_name(&self, user_id: Rc<String>) -> Rc<String> {
        self.users.get_display_name(user_id)
    }

    pub fn update_member(&mut self, member: MemberEvent) {
        self.users.update_member(member);
    }
}

#[derive(Debug,Default)]
struct UserList {
    users: HashMap<Rc<String>, UserState>,
    // user ids by display name
    display_name_users: HashMap<Rc<String>, HashSet<Rc<String>>>,
}

#[derive(Debug,Default)]
struct UserState {
    display_name: Option<Rc<String>>,
    avatar_url: Option<String>,
}

impl UserList {
    pub fn update_member(&mut self, member: MemberEvent) {
        let user_id = Rc::new(member.user);
        let display_name = member.display_name.map(Rc::new);

        match member.membership {
            Membership::Join => self.join(user_id, display_name, member.avatar_url),
            Membership::Leave => self.leave(user_id),
            Membership::Ban => self.ban(user_id),
            Membership::Knock => {},
            Membership::Invite => {},
        }
    }

    pub fn join(&mut self, user_id: Rc<String>, display_name: Option<Rc<String>>, avatar_url: Option<String>) {
        let user = self.users.entry(user_id.clone()).or_insert_with(UserState::default);
        user.avatar_url = avatar_url;
        
        // Replace display name
        if user.display_name != display_name {
            if let Some(display_name) = display_name.clone() {
                Self::add_display_name_user(&mut self.display_name_users, user_id.clone(), display_name);
            }

            let old_display_name = user.display_name.take();
            user.display_name = display_name.clone();

            if let Some(old_display_name) = old_display_name {
                Self::remove_display_name_user(&mut self.display_name_users, user_id, old_display_name);  
            }
        }
    }

    // TODO: Maybe use &String instead of Rc<String> here?
    pub fn leave(&mut self, user_id: Rc<String>) {
        let user = self.users.remove(&user_id);
        if let Some(display_name) = user.and_then(|user| user.display_name) {
            Self::remove_display_name_user(&mut self.display_name_users, user_id, display_name);
        }
    }

    /// Alias to `leave` at the moment
    pub fn ban(&mut self, user_id: Rc<String>) {
        self.leave(user_id);
    }

    fn add_display_name_user(display_name_users: &mut HashMap<Rc<String>, HashSet<Rc<String>>>, user_id: Rc<String>, display_name: Rc<String>) {
        display_name_users
            .entry(display_name)
            .or_insert_with(HashSet::default)
            .insert(user_id);
    }

    fn remove_display_name_user(display_name_users: &mut HashMap<Rc<String>, HashSet<Rc<String>>>, user_id: Rc<String>, display_name: Rc<String>) {
        let n_ids = display_name_users.get_mut(&display_name).map(|ids| {
            ids.remove(&user_id);
            ids.len()
        });

        if n_ids == Some(0) {
            display_name_users.remove(&display_name);
        }
    }

    /// Returns the disambiguated display name of `user_id` i.e. one of three things:
    ///
    /// - The display name if it was found and noone else uses it
    /// - The `user_id` if no display name is found
    /// - The display name in the form of `display name (user id)` when the name is already used
    pub fn get_display_name(&self, user_id: Rc<String>) -> Rc<String> {
        let display_name = self.users.get(&user_id).and_then(|u| u.display_name.clone());
        if let Some(display_name) = display_name {
            let n_display_name_users = self.display_name_users.get(&display_name).map(HashSet::len).unwrap_or(0);
            if n_display_name_users <= 1 {
                display_name
            } else {
                Rc::new(format!("{display_name} ({user_id})",
                    display_name = display_name,
                    user_id = user_id,
                ))
            }
        } else {
            user_id
        }
    }
}
